## The Basics

`Unformatted, monospace`  
*Italics*  
**Bold**  

```markdown
`Inline code`  
*Italics*  
**Bold**  
```

## Headers

```markdown
# Header 1
## Header 2
### And so on, up to 6 `#`s
```

Alternatively:

```markdown
Header 1
--------

Header 2
========
```

## Code

Blocks of code (or preformatted text) uses 3 backticks.  
The highlighter will, usually, figure out which language is in the block.  
However, you can follow the first three backticks with the language name for clarification.  
You can also use `none` as the language to disable highlighting.

```c++
int main()
{
  for (int i : [1,2,3])
  {
    std::cout << i << '\n';
  }
}
```

```markdown
```c++
int main()
{
  for (int i : [1,2,3])
  {
    std::cout << i << '\n';
  }
}
```
```

Lists
-----

The above header should be rendered as an H2 tag.  

Code:
```markdown
	We all like making lists
	------------------------
```
Now, for a list of fruits:

* Red Apples
* Purple Grapes
* Green Kiwifruits

Code:
```markdown
Now, for a list of fruits:

* Red Apples
* Purple Grapes
* Green Kiwifruits
```

Lists with paragraphs:

1.  This is a list item with two paragraphs. Lorem ipsum dolor
    sit amet, consectetuer adipiscing elit. Aliquam hendrerit
    mi posuere lectus.

    Vestibulum enim wisi, viverra nec, fringilla in, laoreet
    vitae, risus. Donec sit amet nisl. Aliquam semper ipsum
    sit amet velit.

2.  Suspendisse id sem consectetuer libero luctus adipiscing.

Code:
```markdown
Lists with paragraphs:

1.  This is a list item with two paragraphs. Lorem ipsum dolor
    sit amet, consectetuer adipiscing elit. Aliquam hendrerit
    mi posuere lectus.

    Vestibulum enim wisi, viverra nec, fringilla in, laoreet
    vitae, risus. Donec sit amet nisl. Aliquam semper ipsum
    sit amet velit.

2.  Suspendisse id sem consectetuer libero luctus adipiscing.
```

What about some code **in** a list? That's insane, right?

1. In Ruby you can map like this:

        ['a', 'b'].map { |x| x.uppercase }

2. In Rails, you can do a shortcut:

        ['a', 'b'].map(&:uppercase)

Code:
```markdown
What about some code **in** a list? That's insane, right?

1. In Ruby you can map like this:

        ['a', 'b'].map { |x| x.uppercase }

2. In Rails, you can do a shortcut:

        ['a', 'b'].map(&:uppercase)

```

More code...
------------

Maybe you want to print `robot` to the console 1000 times. Why not?

    def robot_invasion
      puts("robot " * 1000)
    end

Code:
```none
    def robot_invasion
      puts("robot " * 1000)
    end
```

You see, that was formatted as code because it's been indented by four spaces.

How about we throw some angle braces and ampersands in there?

    <div class="footer">
        &copy; 2004 Foo Corporation
    </div>

Code:
```none
    <div class="footer">
        &copy; 2004 Foo Corporation
    </div>
```

Preformatting
-------------

Preformatted blocks are useful for ASCII art:

<pre>
             ,-.
    ,     ,-.   ,-.
   / \   (   )-(   )
   \ |  ,.>-(   )-<
    \|,' (   )-(   )
     Y ___`-'   `-'
     |/__/   `-'
     |
     |
     |    -hrr-
  ___|_____________
</pre>

Code:
```html
<pre>
             ,-.
    ,     ,-.   ,-.
   / \   (   )-(   )
   \ |  ,.>-(   )-<
    \|,' (   )-(   )
     Y ___`-'   `-'
     |/__/   `-'
     |
     |
     |    -hrr-
  ___|_____________
</pre>
```

You can also just use a code block with the `none` language.

Quotation
---------

If you need to blame someone, the best way to do so is by quoting them:

> I, at any rate, am convinced that He does not throw dice.

Code:
```markdown
> I, at any rate, am convinced that He does not throw dice.
```

Or perhaps someone a little less eloquent:

> I wish you'd have given me this written question ahead of time so I
> could plan for it... I'm sure something will pop into my head here in
> the midst of this press conference, with all the pressure of trying to
> come up with answer, but it hadn't yet...
>
> I don't want to sound like
> I have made no mistakes. I'm confident I have. I just haven't - you
> just put me under the spot here, and maybe I'm not as quick on my feet
> as I should be in coming up with one.

Code:
```markdown
> I wish you'd have given me this written question ahead of time so I
> could plan for it... I'm sure something will pop into my head here in
> the midst of this press conference, with all the pressure of trying to
> come up with answer, but it hadn't yet...
>
> I don't want to sound like
> I have made no mistakes. I'm confident I have. I just haven't - you
> just put me under the spot here, and maybe I'm not as quick on my feet
> as I should be in coming up with one.
```

Tables
------

ID |  Name  | Rank
---|:------:|------:
1  | Tom Preston-Werner | Awesome
2  | Albert Einstein | Nearly as awesome

Code:
```markdown
ID |  Name  | Rank
---|:------:|------:
1  | Tom Preston-Werner | Awesome
2  | Albert Einstein | Nearly as awesome
```

Linking
-------

I get 10 times more traffic from [Google][1] than from
[Yahoo][2] or [MSN][3].

  [1]: http://google.com/        "Google"
  [2]: http://search.yahoo.com/  "Yahoo Search"
  [3]: http://search.msn.com/    "MSN Search"

Code:
```markdown
I get 10 times more traffic from [Google][1] than from
[Yahoo][2] or [MSN][3].

  [1]: http://google.com/        "Google"
  [2]: http://search.yahoo.com/  "Yahoo Search"
  [3]: http://search.msn.com/    "MSN Search"
```

Images
------

Here's an image.

![This is an image](http://localhost:8085/app.png)

Code:
```markdown
![This is an image](http://localhost:8085/app.png)
```

Note: to use images on a landing page (index.md), prefix the image URL with the name of the directory it appears in, omitting the numerical prefix used to order the sections. For example in this section, to display this image on the landing page (index.md), the URL for the image would be "Features/sampleimage.png" to display the same image.
